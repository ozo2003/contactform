<?php

ob_start();
session_start();

define('DOCROOT', dirname(__FILE__));
require_once(DOCROOT."/classes/config/static_config.php");
function autoLoader($class){
	if(is_file(DOCROOT."/classes/".$class.".php")){
		require_once(DOCROOT."/classes/".$class.".php");
	} elseif(is_file(DOCROOT."/classes/system/".$class.".php")){
		require_once(DOCROOT."/classes/system/".$class.".php");
	} elseif(is_file(DOCROOT."/classes/local/".$class.".php")){
		require_once(DOCROOT."/classes/local/".$class.".php");
	} else {
		try {
			require_once(DOCROOT."/classes/".$class.".php");
		} catch(Exception $e){
			throw new Exception("Unable to load ".$class.".");
		}
	}
}
spl_autoload_register('autoLoader');
require_once(DOCROOT."/classes/config/dynamic_config.php");

function __($name){
	return Crypt::encrypt($name);
}

RainTPL::$tpl_dir = "templates/";
RainTPL::$cache_dir = "tmp/";

$main = new RainTPL();
$page = new RainTPL();

SetPage::DefaultsPre();

if (
	!file_exists('templates/'.SetPage::$getpage.'.html') || !SetPage::$getpage
) {
	header('Location: '.HOST.'home');
}

try {
	$PDO = new PDOc(DBName,DBUser,DBPass,DBHost);
	$pdo = $PDO->pdo;
} catch(Exception $e){
	echo $e->getMessage();
}

$post = NULL;
if(Input::posts()){
	$post = SetPage::Post();
}

SetPage::DefaultsPost();

if(SetPage::$getpage=='home'){
	SetPage::Page(SetPage::$getpage);

	SetPage::MainPage();
}

if($post){
	User::printStats();
}