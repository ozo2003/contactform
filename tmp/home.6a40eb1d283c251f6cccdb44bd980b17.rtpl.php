<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("menu") . ( substr("menu",-1,1) != "/" ? "/" : "" ) . basename("menu") );?>

<div id="random_text">
	<form method="POST">
		<div class="input-group">
			<span class="input-group-addon" id="name-addon">Name</span>
			<input type="text" class="form-control" aria-describedby="name-addon" name="contact_name" required />
		</div>
		<div class="input-group">
			<span class="input-group-addon" id="email-addon">E-mail</span>
			<input type="email" class="form-control" aria-describedby="email-addon" name="contact_email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$" required/>
		</div>
		<div class="input-group">
			<span class="input-group-addon" id="phone-addon">Phone</span>
			<input type="text" class="form-control" aria-describedby="phone-addon" name="contact_phone" required />
		</div>
		<div class="input-group">
			<span class="input-group-addon" id="city-addon">City</span>
			<select class="cities_select" aria-describedby="city-addon" name="contact_city" required>
				<option selected="selected"></option>
				<?php $counter1=-1; if( isset($cities) && is_array($cities) && sizeof($cities) ) foreach( $cities as $key1 => $value1 ){ $counter1++; ?>

					<option value="<?php echo $value1["id"];?>"><?php echo $value1["city"];?></option>
				<?php } ?>

			</select>
		</div>
		<div class="btn-group" role="group">
			<input type="submit" class="btn btn-default" />
			<button type="reset" class="btn btn-default" id="reset">Clear</button>
		</div>
	</form>
</div>