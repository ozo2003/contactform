<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html>
	<head>
		<base href="<?php echo $base_url;?>">
		<?php $counter1=-1; if( isset($css) && is_array($css) && sizeof($css) ) foreach( $css as $key1 => $value1 ){ $counter1++; ?>
		<link rel="stylesheet" type="text/css" href="templates/css/<?php echo $value1;?>">
		<?php } ?>
		<?php $counter1=-1; if( isset($js) && is_array($js) && sizeof($js) ) foreach( $js as $key1 => $value1 ){ $counter1++; ?>
		<script type="text/javascript" src="templates/js/<?php echo $value1;?>"></script>
		<?php } ?>
		<title>Contact Form</title>
		<link rel="shortcut icon" type="image/png" href="templates/favicon.png" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<div id="main">
			<?php echo $content;?>
		</div>
	</body>
</html>