<?php

class PDOc {

	var $dbhost;
	var $dbuser;
	var $dbpass;
	var $dbname;

	var $result;
	var $row;
	var $data;
	var $lastId;
	var $pdo;

	public function PDOc($dbname, $dbuser, $dbpass, $dbhost = "localhost"){
		$this->dbname = $dbname;
		$this->dbuser = $dbuser;
		$this->dbpass = $dbpass;
		$this->dbhost = $dbhost;
		$this->Connect();
	}

	private function Connect() {
		try {
			$this->pdo = new PDO("mysql:host=".$this->dbhost.";dbname=".$this->dbname, $this->dbuser, $this->dbpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(Exception $e) {
			Input::error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function quote($value){
		global $pdo;
		return $pdo->quote($value);
	}

	public static function getData($query, $data = NULL){
		$result = self::executeSQL($query, $data);
		$data = NULL;
		$i = 0;
		while($row = $result->fetch(PDO::FETCH_ASSOC)){
			$data[$i] = $row;
			$i++;
		}
		return $data;
	}

	public static function getData2($query, $data = NULL){
		$result = self::executeSQL($query, $data);
		$data = NULL;
		$i = 0;
		while($row = $result->fetch(PDO::FETCH_ASSOC)){
			$data[$i] = $row;
			$i++;
		}
		return $data;
	}

	public static function executeSQL($query, $data = NULL){
		global $pdo;
		$pdo->beginTransaction();
		$id = FALSE;
		try {
			if(!$data){
				$result = $pdo->query($query);
			} else {
				$result = $pdo->prepare($query);
				$result->execute($data);
			}
			$id = $pdo->lastInsertId();
			$pdo->commit();
		} catch (Exception $e){
			$pdo->rollBack();
			Input::error_msg(__FUNCTION__, $e->getMessage());
		}
		return $result;
	}

	public static function executeSQL2($query, $data = NULL, $str = NULL){
		global $pdo;
		$pdo->beginTransaction();
		$id = NULL;
		try {
			$result = $pdo->prepare($query);
			$i = 1;
			foreach($data AS $key){
				if($key == $str){
					$result->bindParam($i, $key, PDO::PARAM_LOB);
				} else {
					$result->bindParam($i, $key);
				}
				$i++;
			}
			$result->execute();
			$id = $pdo->lastInsertId();
			$pdo->commit();
		} catch (Exception $e){
			$pdo->rollBack();
			Input::error_msg(__FUNCTION__, $e->getMessage());
		}
		return $result;
	}

	public static function setForeignChecks($set = 0){
		$query = "SET foreign_key_checks = ?";
		self::executeSQL($query, array($set));
	}

}