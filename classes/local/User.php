<?php

class User {

	private static function getStats(){
		global $pdo;

		$query = "
			SELECT
				c.city,
				COUNT(u.city_id) AS counter
			FROM
				cities c
			LEFT JOIN
				users u
					ON u.city_id = c.id
			GROUP BY
				c.city
			ORDER BY
				counter DESC, c.city
		";
		return PDOc::getData($query);
	}

	private static function saveDB($data){
		global $pdo;

		$query = "
			INSERT INTO
				users
					(name, email, phone, city_id)
			VALUES
				(:name, :email, :phone, :city)
		";
		PDOc::executeSQL($query,$data);
	}

	public static function saveContact(){
		$data = NULL;
		$fields = array("name", "email", "phone", "city");
		foreach($fields AS $field){
			$data[$field] = Input::post('contact_'.$field);
		}
		$result = self::saveDB($data);

		return 1;
	}

	public static function printStats(){
		$data = self::getStats();

		echo '<br /></br />
			<div class="panel panel-default">
			  <div class="panel-heading">Stats</div>
			  <div class="panel-body">
			  <table class="table">
			  	<tr>
			  		<th>City</th>
			  		<th>Count</th>
			  	<tr>
		';
		foreach($data AS $city){
			echo '<tr><td>'.$city['city'].'</td><td>'.$city['counter'].'</td></tr>';
		}
		echo '
				</table>
			  </div>
			</div>
		';
	}

	public static function getCitiesData(){
		global $pdo;

		$query = "
			SELECT
				*
			FROM
				cities
		";
		return PDOc::getData($query);
	}

}