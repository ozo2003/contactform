<?php

class SetPage {

	public static $getpage = NULL;
	public static $selfpage;
	public static $menu;

	public static function Post(){
		global $page;

		$result = NULL;
		if(Input::posts()){
			$result = User::saveContact();
		}

		return $result;
	}

	public static function MainPage(){
		global $main;
		global $page;

		$css = NULL;
		$js = NULL;

		foreach(glob('templates/css/*.css') AS $filename){
			$file = explode("/",$filename);
			$file = end($file);
			$css[] = $file;
		}

		foreach(glob("templates/js/*.js") AS $filename){
			$file = explode("/",$filename);
			$file = end($file);
			$js[] = $file;
		}

		$main->assign('css', $css);
		$main->assign('js', $js);
		$main->assign('base_url', Functions::Host());
		$main->assign('getpage', self::$getpage);
		$main->assign('content', $page->draw(SetPage::$getpage, $return_string=true));
		$main->draw('main');
	}

	public static function Page($get = 'home'){
		global $page;

		switch($get){
			case 'home': self::setHome(); break;
		}
	}

	public static function DefaultsPre(){
		self::$getpage = rtrim(Input::get("opt"),'/');
		$self = explode("/",self::$getpage);
		self::$selfpage = $self[EXPLODE];
	}

	public static function DefaultsPost(){
		global $page;

		$page->assign('getpage', self::$getpage);
	}

	public static function setHome(){
		global $page;		

		$cities = User::getCitiesData();
		$page->assign("cities", $cities);

	}

}
