<?php

class Functions {

	public static function Host() {
		$host = $_SERVER['HTTP_HOST'];
		if($_SERVER['SRV_PROTOCOL'] == 'http'){
			if (substr($host, 0, 7) != 'http://') {
				$host = 'http://' . $host;
			}
		} elseif($_SERVER['SRV_PROTOCOL'] == 'https'){
			if (substr($host, 0, 8) != 'https://') {
				$host = 'https://' . $host;
			}
		}
		$host .= ROOT;
		return $host;
	}

	public static function unId($len = 5) {
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
		return substr(str_shuffle($str), 0, $len);
	}

	public static function checkEmail($email) {
		$regex = "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";
		return preg_match($regex, $email) ? 1 : 0;
	}

	public static function isArrayInside($array){
	    foreach($array as $value){
	        if(is_array($value)) {
	          return true;
	        }
	    }
	    return false;
	}
	
}