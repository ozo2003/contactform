<?php

/*Static*/
date_default_timezone_set("Europe/Riga");
error_reporting(E_ALL | E_STRICT); 
ini_set("session.gc_maxlifetime", "7200");
ini_set("display_errors", 1); 
define("ROOT", "/cont/");
define("SYSTEM_ID", 1);
define("SALT", substr(sha1('qasdfgdsdfghjnbvcdfghnbvfghjnbvfghnbvgh'),0,30));
define("EXPLODE", 0);
define("IP", $_SERVER["REMOTE_ADDR"]);

/*DB*/
define("DBHost","localhost");
define("DBUser","root");
define("DBPass","P@ssw0rd");
define("DBName","contacts");